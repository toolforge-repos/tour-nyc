// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Hash, PartialEq, Eq, Clone)]
pub struct DidYouKnow {
    pub hook: String,
    pub timestamp: String,
}

#[derive(Debug, Serialize, Deserialize, Hash, PartialEq, Eq, Clone)]
pub struct Article {
    pub title: String,
    pub summary: Option<String>,
    pub good: bool,
    pub featured: bool,
    pub is_ny: bool,
    pub image: Option<String>,
    pub did_you_know: Option<DidYouKnow>,
}
