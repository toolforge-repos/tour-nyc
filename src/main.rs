// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
use anyhow::Result;
use mwbot::parsoid::{Wikicode, WikinodeIterator};
use mwbot::Bot;
use regex::Regex;
use std::collections::{BTreeSet, HashMap};
use std::sync::OnceLock;
use tokio::fs;
use tour_nyc::{Article, DidYouKnow};

static DYK_RE: OnceLock<Regex> = OnceLock::new();

fn munge_html(input: String) -> String {
    input.replace("href=\"./", "href=\"https://en.wikipedia.org/wiki/")
}

async fn lookup_dyk(
    bot: Bot,
    oldid: u64,
    title: String,
    timestamp: String,
) -> Result<Option<(String, DidYouKnow)>> {
    //println!("Looking up credit for {title} @ oldid={oldid}...");
    let code = bot
        .page("User talk:Epicgenius")?
        .revision_html(oldid)
        .await?
        .into_mutable();
    for italic in code.select("i").iter().rev() {
        let mut html = "".to_string();
        if !italic.text_contents().starts_with("... that") {
            continue;
        }
        html.push_str(&italic.to_string());
        if !italic.text_contents().ends_with('?') {
            // Could be split because of e.g (pictured) not being in italics
            for node in italic.following_siblings() {
                html.push_str(&node.to_string());
                if node.text_contents().ends_with('?') {
                    break;
                }
            }
        }

        // Check that it actually links to the title we want
        let hook_code = Wikicode::new(&html);
        if !hook_code
            .filter_links()
            .iter()
            .any(|link| link.target() == title)
        {
            continue;
        }
        // Normalize the title
        let title = bot.page(&title)?.title().to_string();
        return Ok(Some((
            title,
            DidYouKnow {
                hook: munge_html(html),
                timestamp,
            },
        )));
    }
    println!("Couldn't find hook for {title} @ oldid={oldid}");
    Ok(None)
}

async fn fetch_dyks(bot: &Bot) -> Result<HashMap<String, DidYouKnow>> {
    let result = bot
        .api()
        .http_client()
        .get("https://betacommand-dev.toolforge.org/reports/logs/dyk/Epicgenius.html")
        .send()
        .await?
        .error_for_status()?;
    let regex = DYK_RE.get_or_init(|| {
        Regex::new(
            r#"oldid=(\d*?)&diff=prev">(\d{14})</a></td>\n\s*?<td>Giving DYK credit for \[\[(.*?)\]\] on behalf"#,
        ).unwrap()
    });
    let mut handles = vec![];
    let text = result.text().await?;
    for cap in regex.captures_iter(&text) {
        let oldid: u64 = cap[1].parse().unwrap();
        let timestamp = cap[2].to_string();
        let title = cap[3].replace("&#8211;", "–").to_string();
        let bot = bot.clone();
        handles.push(tokio::spawn(async move {
            lookup_dyk(bot, oldid, title, timestamp).await
        }));
    }
    let mut dyks = HashMap::new();
    for handle in handles {
        if let Some((title, dyk)) = handle.await?? {
            println!("Pulled DYK hook for {title}");
            dyks.insert(title, dyk);
        }
    }
    Ok(dyks)
}

async fn fetch_article(bot: Bot, title: String) -> Result<Article> {
    let page = bot.page(&title)?;
    // Follow redirect
    let page = page.redirect_target().await?.unwrap_or(page);
    let code = page.html().await?;
    let talk_code = bot.page(&format!("Talk:{}", page.title()))?.html().await?;
    let mut featured = false;
    let mut good = false;
    let code = code.into_mutable();
    for indicator in code.descendants().filter_map(|node| node.as_indicator()) {
        let name = indicator.name().unwrap_or("".to_string());
        if name == "featured-star" {
            featured = true;
        } else if name == "good-star" {
            good = true;
        }
        if good && featured {
            break;
        }
    }

    // Find the first image in the infobox
    // TODO: use page image?
    let image =
        code.select_first(".infobox a.mw-file-description img")
            .map(|img| {
                if let Some(element) = img.as_element() {
                    // TODO: do something based on whether it's vertical or horizontal
                    let _height: u64 = element
                        .attributes
                        .borrow_mut()
                        .remove("height")
                        .unwrap()
                        .value
                        .parse()
                        .unwrap();
                    let _width: u64 = element
                        .attributes
                        .borrow_mut()
                        .remove("width")
                        .unwrap()
                        .value
                        .parse()
                        .unwrap();
                    element
                        .attributes
                        .borrow_mut()
                        .insert("class", "card-img-top".to_string());
                }
                img.to_string()
            });

    // Find and extract the first paragraph. Look for the first paragraph with un-templated
    // bold text
    let summary = code
        .select_first("section[data-mw-section-id=\"0\"] > p > b")
        .map(|bold| {
            // unwrap: Safe because the selector ensures there's a parent
            let parent = bold.parent().unwrap();
            // We want all children's HTML without the <p>
            let html = parent
                .children()
                .map(|child| child.to_string())
                .collect::<Vec<_>>()
                .join("");
            munge_html(html)
        });

    // Make sure it's NYC-related
    // TODO: less terrible plz
    let is_ny = talk_code.html().contains("WikiProject New York City")
        || talk_code.html().contains("WikiProject New York (state)");

    Ok(Article {
        title: page.title().to_string(),
        summary,
        good,
        featured,
        image,
        is_ny,
        did_you_know: None,
    })
}

async fn fetch_quality(
    bot: &Bot,
    mut dyks: HashMap<String, DidYouKnow>,
) -> Result<Vec<Article>> {
    let mut links = BTreeSet::new();
    let code = bot
        .page("User:Epicgenius/Quality article contributions")?
        .html()
        .await?
        .into_mutable();
    // XXX: Is the GAN list at https://sdzerobot.toolforge.org/gans?user=Epicgenius useful?
    for section in code.iter_sections() {
        if let Some(heading) = section.heading() {
            if heading.text_contents() == "Good and featured articles" {
                for link in section.filter_links() {
                    links.insert(link.target());
                }
            }
        }
    }
    links.extend(dyks.keys().map(|k| k.to_string()));
    let mut handles = vec![];
    for title in links {
        let bot = bot.clone();
        handles
            .push(tokio::spawn(async move { fetch_article(bot, title).await }));
    }
    let mut articles = vec![];
    for handle in handles {
        let mut article = handle.await??;
        println!("Looked up information about {}", &article.title);
        article.did_you_know = dyks.remove(&article.title);
        articles.push(article);
    }

    Ok(articles)
}

async fn follow_dyk_redirects(
    bot: &Bot,
    dyks: HashMap<String, DidYouKnow>,
) -> Result<HashMap<String, DidYouKnow>> {
    let mut ret = HashMap::new();
    for (title, dyk) in dyks {
        let page = bot.page(&title)?;
        let page = page.redirect_target().await?.unwrap_or(page);
        ret.insert(page.title().to_string(), dyk);
    }
    Ok(ret)
}

#[tokio::main]
async fn main() -> Result<()> {
    let bot = Bot::from_default_config().await?;
    let dyks = fetch_dyks(&bot).await?;
    println!("Found {} DYKs", dyks.len());
    let dyks = follow_dyk_redirects(&bot, dyks).await?;
    println!("Resolved DYK redirects");
    let articles = fetch_quality(&bot, dyks).await?;
    dbg!(&articles);
    fs::write("data.json", serde_json::to_string(&articles)?).await?;
    Ok(())
}
