// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
#[macro_use]
extern crate rocket;

use anyhow::Result;
use rocket::response::content::RawJson;
use rocket_dyn_templates::Template;
use rocket_healthz::Healthz;
use serde::Serialize;
use std::collections::HashSet;
use tokio::fs;
use tour_nyc::Article;

/// The context needed to render the "index.html" template
#[derive(Serialize)]
struct IndexTemplate {
    data: Vec<Article>,
    stats: Stats,
}

#[derive(Serialize, Default)]
struct Stats {
    dyks: usize,
    featured: usize,
    good: usize,
}

async fn data() -> Result<Vec<Article>> {
    let contents = fs::read_to_string("data.json").await?;
    // "shuffle" by using a HashSet
    let articles: HashSet<Article> = serde_json::from_str(&contents)?;
    Ok(articles.into_iter().filter(|a| a.is_ny).collect())
}

#[get("/")]
async fn index() -> Template {
    let data = data().await.unwrap();
    let mut stats = Stats::default();
    let mut snowdaddy = None;
    for article in &data {
        if article.featured {
            stats.featured += 1;
        }
        if article.good {
            stats.good += 1;
        }
        if article.did_you_know.is_some() {
            stats.dyks += 1;
        }
        if article.title == "Bust of Edward Snowden" {
            snowdaddy = Some(article.clone());
        }
    }
    let data: Vec<_> = data.into_iter().take(50).collect();
    let data = if snowdaddy.is_some()
        && !data.iter().any(|a| a.title == "Bust of Edward Snowden")
    {
        let mut new: HashSet<_> = data.into_iter().take(49).collect();
        new.insert(snowdaddy.unwrap());
        new.into_iter().collect()
    } else {
        data
    };

    Template::render("index", IndexTemplate { data, stats })
}

#[get("/data.json")]
async fn raw_data() -> RawJson<String> {
    RawJson(fs::read_to_string("data.json").await.unwrap())
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .mount("/", routes![index, raw_data])
        .attach(Template::fairing())
        .attach(Healthz::fairing())
}
